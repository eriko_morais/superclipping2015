﻿using System.Collections.Generic;
using SuperClipping.Domain.Interfaces;

namespace SuperClipping.Domain.Entities
{
    public class Noticia 
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public string Resumo { get; set; }
        public int IdVeiculo { get; set; }
        public virtual Veiculo Veiculo { get; set; }
        public virtual IEnumerable<Anexo> Anexos { get; set; }
        public virtual IEnumerable<Assunto> Assuntos { get; set; }
    }
}
